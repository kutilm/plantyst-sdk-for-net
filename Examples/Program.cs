﻿namespace Example
{
    using System;
    using System.IO;
    using Plantyst.Client;
    using Plantyst.Client.Security;

    class Program
    {
        static void Main(string[] args)
        {
            // Setup
            var userAccessToken = new UserAccessToken
            {
                Application = "",
                Key = @"",
                Secret = @""
            };

            var result = CallMeasurementClient(userAccessToken);

            Console.WriteLine(result);
            Console.ReadKey();
        }

        private static string CallMeasurementClient(UserAccessToken userAccessToken)
        {
            // Call api
            var measurementClient = new MeasurementClient(userAccessToken);
            var task = measurementClient.GetAsync();
            task.Wait();

            // Process result
            var stream = task.Result;
            return new StreamReader(stream).ReadToEnd();
        }
    }
}
