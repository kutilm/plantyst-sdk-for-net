﻿namespace Plantyst.Client
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Net;
    using System.Security.Claims;
    using System.Web;
    using Plantyst.Client.Security;

    /// <summary>
    /// The http web request factory.
    /// </summary>
    public class RequestFactory
    {
        /// <summary>
        /// The base URI.
        /// </summary>
        private string baseUri = @"https://my.plantyst.com/api/";

        /// <summary>
        /// The authorization token provider.
        /// </summary>
        private AuthorizationTokenProvider authorizationTokenProvider = new AuthorizationTokenProvider();

        /// <summary>
        /// Gets or sets the base URI.
        /// </summary>
        /// <value>
        /// The base URI.
        /// </value>
        public string BaseUri
        {
            get
            {
                return this.baseUri;
            }

            set
            {
                this.baseUri = value;
            }
        }

        /// <summary>
        /// Creates the get request.
        /// </summary>
        /// <param name="relativeUri">The relative URI.</param>
        /// <param name="userAccessToken">The user access token.</param>
        /// <param name="parameters">The query string parameters.</param>
        /// <param name="claims">The claims add to the Authorization token header.</param>
        /// <returns>Http web request.</returns>
        /// <exception cref="System.ArgumentException">Throw if argument <see cref="relativeUri"/> is null or empty.</exception>
        public virtual HttpWebRequest CreateGetRequest(string relativeUri, IUserAccessToken userAccessToken, NameValueCollection parameters, IEnumerable<Claim> claims = null)
        {
            if (string.IsNullOrWhiteSpace(relativeUri))
            {
                throw new ArgumentException("Argument relativeUri can not be null or empty.", "relativeUri");
            }

            var uriBuilder = new UriBuilder(this.BaseUri);
            uriBuilder.Path += relativeUri;

            // Add parameters to the request.
            if (parameters != null && parameters.Count > 0)
            {
                var queryString = HttpUtility.ParseQueryString(uriBuilder.Query);
                queryString.Add(parameters);
                uriBuilder.Query = queryString.ToString();
            }

            // Create HttpWebRequest.
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uriBuilder.Uri);
            request.Method = "GET";

            // Add authorization header
            if (userAccessToken != null)
            {
                var token = this.authorizationTokenProvider.CreateAuthorizationToken(userAccessToken, claims);
                request.Headers.Add("Authorization", "Token " + token);
                request.Accept = "application/hal+json";
            }

            return request;
        }
    }
}
