﻿namespace Plantyst.Client.Security
{
    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Protocols.WSTrust;
    using System.IdentityModel.Tokens;
    using System.Security.Claims;

    /// <summary>
    /// Authorization token provider.
    /// </summary>
    public class AuthorizationTokenProvider
    {
        /// <summary>
        /// The default life time length in seconds.
        /// </summary>
        public const int DefaultLifeTimeLength = 60;

        /// <summary>
        /// The intended for URL.
        /// </summary>
        public const string IntendedFor = @"https://my.plantyst.com/";

        /// <summary>
        /// The creation offset lifetime in seconds.
        /// </summary>
        private const int CreationOffset = -5;

        /// <summary>
        /// The life time provider.
        /// </summary>
        private Func<Lifetime> lifeTimeProvider = () =>
        {
            var now = DateTime.UtcNow;
            return new Lifetime(now.AddSeconds(CreationOffset), now.AddSeconds(DefaultLifeTimeLength));
        };

        /// <summary>
        /// Gets or sets the life time provider.
        /// </summary>
        public Func<Lifetime> LifeTimeProvider
        {
            get
            {
                return this.lifeTimeProvider;
            }

            set
            {
                this.lifeTimeProvider = value;
            }
        }

        /// <summary>
        /// Creates the authorization token with specified lifetime.
        /// </summary>
        /// <param name="userAccessToken">The user access token.</param>
        /// <param name="claims">The token claims.</param>
        /// <param name="lifetime">The token lifetime.</param>
        /// <returns>The authorization token.</returns>
        public string CreateAuthorizationToken(IUserAccessToken userAccessToken, IEnumerable<Claim> claims = null, Lifetime lifetime = null)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var subject = new ClaimsIdentity(claims);
            subject.AddClaim(new Claim("AccessKey", userAccessToken.Key));

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = subject,
                TokenIssuerName = userAccessToken.Application,
                AppliesToAddress = AuthorizationTokenProvider.IntendedFor,
                Lifetime = lifetime ?? this.LifeTimeProvider(),
                SigningCredentials = new SigningCredentials(
                    new InMemorySymmetricSecurityKey(Convert.FromBase64String(userAccessToken.Secret)),
                    "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256",
                    "http://www.w3.org/2001/04/xmlenc#sha256")
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            return tokenString;
        }
    }
}
