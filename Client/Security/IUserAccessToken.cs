﻿namespace Plantyst.Client.Security
{
    /// <summary>
    /// User access token.
    /// </summary>
    public interface IUserAccessToken
    {
        /// <summary>
        /// Gets the application id.
        /// </summary>
         string Application { get; }

         /// <summary>
         /// Gets the access token key.
         /// </summary>
         string Key { get; }

         /// <summary>
         /// Gets the access token secret.
         /// </summary>
         /// <remarks>The secret is between byte array and the string coded/decoded by the base64.</remarks>
         string Secret { get; }
    }
}
