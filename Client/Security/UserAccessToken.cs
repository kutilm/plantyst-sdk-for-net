﻿namespace Plantyst.Client.Security
{
    /// <summary>
    /// User access token.
    /// </summary>
    public struct UserAccessToken : IUserAccessToken
    {
        /// <summary>
        /// Gets or sets the application.
        /// </summary>
        public string Application { get; set; }

        /// <summary>
        /// Gets or sets access token key.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the access token secret.
        /// </summary>
        /// <remarks>The secret is between byte array and the string coded/decoded by the base64.</remarks>
        public string Secret { get; set; }
    }
}
