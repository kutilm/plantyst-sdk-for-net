﻿namespace Plantyst.Client
{
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Net;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Plantyst.Client.Security;

    /// <summary>
    /// Downloads measurements from server.
    /// </summary>
    public class MeasurementClient
    {
        /// <summary>
        /// The path to the <c>Api</c> access point.
        /// </summary>
        private const string Path = @"Measurements";

        /// <summary>
        /// The date time format.
        /// </summary>
        private const string DateTimeFormat = @"yyyy-MM-dd";

        /// <summary>
        /// The user access token
        /// </summary>
        private IUserAccessToken userAccessToken;

        /// <summary>
        /// The request factory.
        /// </summary>
        private RequestFactory requestFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="MeasurementClient" /> class.
        /// </summary>
        /// <param name="userAccessToken">The user access token.</param>
        public MeasurementClient(IUserAccessToken userAccessToken)
        {
            this.userAccessToken = userAccessToken;
        }

        /// <summary>
        /// Sets the base URI.
        /// </summary>
        /// <value>
        /// The base URI.
        /// </value>
        public string BaseUri
        {
            set
            {
                this.RequestFactory.BaseUri = value;
            }
        }

        /// <summary>
        /// Gets the request factory.
        /// </summary>
        protected virtual RequestFactory RequestFactory
        {
            get
            {
                if (this.requestFactory == null)
                {
                    this.requestFactory = new RequestFactory();
                }

                return this.requestFactory;
            }
        }

        /// <summary>
        /// Gets list of measurements.
        /// </summary>
        /// <returns>
        /// Task with serialized variable data.
        /// </returns>
        public virtual async Task<Stream> GetAsync()
        {
            // prepare parameters
            var parameters = new NameValueCollection();
            parameters.Add("currentSubscriptionOnly", false.ToString());

            HttpWebRequest request = this.RequestFactory.CreateGetRequest(MeasurementClient.Path, this.userAccessToken, parameters);
            var response = await request.GetResponseAsync().ConfigureAwait(false);
            return response.GetResponseStream();
        }
    }
}
